/**
 * @file asauart_slave.c
 * @author Ye cheng-Wei
 * @date 2019.1.22
 * @brief Implement ASA Slave UART Mode0
 *
 *
 */
/*
    FIXME:
    // *  1.狀態機UID沒有回header，須補起來
    *  2.TimerCntStr_inst.timeoutISR_inst[timeout_IsrID].counter =
   TimerCntStr_inst.timeoutISR_inst[timeout_IsrID].time_limit;
    *   須將timeoutISR_inst[timeout_IsrID].counter 設置成一個 static 變數 :
   timeoutISR_inst[timeout_IsrID].time_limit 設置成一個 static 變數
    *   逾時採用判斷兩者大小，當 >= 時，逾時發生。
*/
#include "c4mlib/asabus/src/asabus.h"
#include "c4mlib/asabus/src/remo_reg.h"
#include "c4mlib/debug/src/debug.h"
#include "c4mlib/device/src/device.h"
#include "c4mlib/macro/src/std_res.h"
#include "c4mlib/time/src/timeout.h"

#include "asauart_slave.h"

static uint8_t timeout_IsrID;
static uint8_t timeout_flag = 0;

// UARTS_rx function
void UARTS_rx0() {
    // If SerialIsrStr_init is not call first, There will return
    if (ASAUARTSerialIsrStr == NULL)
        return;

    DEBUG_INFO("UARTS_rx0 call [Timeout:%u]\n", timeout_flag);
    uint8_t data_In;

    switch (ASAUARTSerialIsrStr->sm_status) {
        case UARTS_SM_HEADER:
            /*
                • TOCount=TOut;(逾時計數器恢復)
                • 讀取UART收值與正確領頭碼比判斷是否切換狀態(參考編號1)
            */
            //
            UARTS_Inst.read_byte(&data_In);
            DEBUG_INFO("[UARTS_SM_HEADER]\tread <%02X>\n", data_In);

            if (data_In == CMD_HEADER) {
                //讀取UART收值與正確領頭碼比判斷是否切換狀態(參考編號1)
                ASAUARTSerialIsrStr->sm_status = UARTS_SM_UID;
                ASAUARTSerialIsrStr->check_sum =
                    0;  // Reset the checksum coulmn

                // 開啟逾時中斷
                Timeout_ctl(timeout_IsrID, 1);
            }
            break;

        case UARTS_SM_UID:
            /*
                • 檢查逾時旗標，判斷是否切換狀態(參考編號7)
                • TOCount=TOut;(逾時計數器恢復)
                • 讀取UART收值，做為新UID
                • 依據UID與本裝置是否吻合判斷是否切換狀態(參考編號2)
            */
            if (timeout_flag) {
                // Timeout ISR，回Header (參考編號7)
                ASAUARTSerialIsrStr->sm_status = UARTS_SM_HEADER;
                ASAUARTSerialIsrStr->result_message = HAL_ERROR_TIMEOUT;

                // 關閉逾時中斷
                Timeout_ctl(timeout_IsrID, 0);
                break;
            }
            UARTS_Inst.read_byte(&data_In);
            DEBUG_INFO("[UARTS_SM_UID]\tread <%02X>\n", data_In);

            ASAUARTSerialIsrStr->d_id = data_In;

            // 如果ID與本裝置相同 或 ID為廣播ID:127 (UART_BROADCAST_ID)
            if (ASAUARTSerialIsrStr->d_id == ASAConfigStr_inst.ASA_ID ||
                ASAUARTSerialIsrStr->d_id == UART_BROADCAST_ID) {
                ASAUARTSerialIsrStr->sm_status = UARTS_SM_ADDR;
            }
            else {
                ASAUARTSerialIsrStr->sm_status = UARTS_SM_HEADER;
            }
            ASAUARTSerialIsrStr->check_sum += data_In;
            break;

        case UARTS_SM_ADDR:
            /*
                • TOCount=TOut;
                • 讀取UART收值，分解取bit7為RW，bit6:0為暫存器編號。
                • ChkSum=ChkSum+收值。
                • 檢查RW值決定切換狀態。(參考編號3 或4)
            */
            if (timeout_flag) {  // Timeout ISR，回Header (參考編號7)
                ASAUARTSerialIsrStr->sm_status = UARTS_SM_HEADER;
                ASAUARTSerialIsrStr->result_message = HAL_ERROR_TIMEOUT;

                // 關閉逾時中斷
                Timeout_ctl(timeout_IsrID, 0);
                break;
            }
            UARTS_Inst.read_byte(&data_In);
            DEBUG_INFO("[UARTS_SM_ADDR]\tread <%02X>\n", data_In);

            ASAUARTSerialIsrStr->reg_address = data_In & 0x7f;
            ASAUARTSerialIsrStr->rw_mode = (data_In & 0x80) >> 7;
            if (ASAUARTSerialIsrStr->rw_mode == 0) {  // UART MODE0寫入模式
                ASAUARTSerialIsrStr->sm_status = UARTS_SM_DATA;
            }
            // 檢查RW值決定切換狀態 (參考編號3 或4)
            else if (ASAUARTSerialIsrStr->rw_mode ==
                     1) {  // UART MODE0 讀取模式
                ASAUARTSerialIsrStr->sm_status = UARTS_SM_CHKSUM;
            }

            ASAUARTSerialIsrStr->check_sum += data_In;
            break;

        case UARTS_SM_DATA:
            /*
                • TOCount=TOut;
                • 讀取UART收值，轉存入BUFF(ByteCount  )中。
                • ChkSum=ChkSum+收值。
                • ByteCount =ByteCount +1
                • TotalBytes= RemoRW_reg表第暫存器編號個暫存器Byte數
                • If(ByteCount  ==TotalBytes) ByteCount =0
                • 檢查ByteCount 值決定切換狀態。 (參考編號5)
            */
            if (timeout_flag) {  // Timeout ISR，回Header (參考編號7)
                ASAUARTSerialIsrStr->sm_status = UARTS_SM_HEADER;
                ASAUARTSerialIsrStr->result_message = HAL_ERROR_TIMEOUT;

                // 關閉逾時中斷
                Timeout_ctl(timeout_IsrID, 0);
                break;
            }
            UARTS_Inst.read_byte(&data_In);
            DEBUG_INFO("[UARTS_SM_DATA]\tread <%02X>\n", data_In);
            DEBUG_INFO(
                "byte_counter: <%u>, sz_reg: <%u>\n",
                ASAUARTSerialIsrStr->byte_counter,
                ASAUARTSerialIsrStr->remo_reg[ASAUARTSerialIsrStr->reg_address]
                    .sz_reg);
            ASAUARTSerialIsrStr->temp[ASAUARTSerialIsrStr->byte_counter] =
                data_In;
            ASAUARTSerialIsrStr->byte_counter++;
            // TODO: Check the index(ASAUARTSerialIsrStr->reg_address) is safe
            if (ASAUARTSerialIsrStr->byte_counter ==
                ASAUARTSerialIsrStr->remo_reg[ASAUARTSerialIsrStr->reg_address]
                    .sz_reg) {
                ASAUARTSerialIsrStr->byte_counter = 0;
                ASAUARTSerialIsrStr->sm_status = UARTS_SM_CHKSUM;
            }

            ASAUARTSerialIsrStr->check_sum += data_In;
            break;

        case UARTS_SM_CHKSUM:
            /*
                    • TOCount=TOut;
                    • 讀取UART收值，轉存入主端ChkSum 中。
                    • 比較主端ChkSum與ChkSum決定切換路徑。 (參考編號6，編號8)
                    • IF(RW變數==寫入)
                        ◦ For(ByteCount=0到 TotalBytes )
                            讀取BUFF(ByteCount)轉存入(RemoRW_reg表第暫存器編號個暫存器住址
               +ByteCount )位置。 ◦ Endfor • ELSEIF   RW變數==讀取 Call
               UART_tx() 送第一筆資料。 • ENDIF
                */
            if (timeout_flag) {  // Timeout ISR，回Header (參考編號7)
                ASAUARTSerialIsrStr->sm_status = UARTS_SM_HEADER;
                ASAUARTSerialIsrStr->result_message = HAL_ERROR_TIMEOUT;

                // 關閉逾時中斷
                Timeout_ctl(timeout_IsrID, 0);
                break;
            }
            UARTS_Inst.read_byte(&data_In);
            DEBUG_INFO("[UARTS_SM_CHKSUM]\tread <%02X>\n", data_In);
            DEBUG_INFO("Compute Check_sum = %02X\n",
                       ASAUARTSerialIsrStr->check_sum);
            if (data_In == ASAUARTSerialIsrStr->check_sum) {
                if (ASAUARTSerialIsrStr->rw_mode == 0) {  // Write mode
                    DEBUG_INFO("Process write mode\n");
                    DEBUG_INFO("ASAUARTSerialIsrStr address:%x\n",
                               ASAUARTSerialIsrStr);
                    // Move temporary data in buffer to target register memory
                    for (int i = 0;
                         i < ASAUARTSerialIsrStr
                                 ->remo_reg[ASAUARTSerialIsrStr->reg_address]
                                 .sz_reg;
                         i++) {
                        // printf("temp=%d, ",ASAUARTSerialIsrStr->temp[i]);
                        ASAUARTSerialIsrStr
                            ->remo_reg[ASAUARTSerialIsrStr->reg_address]
                            .data_p[i] = ASAUARTSerialIsrStr->temp[i];
                    }

                    // Process Modify event Callback
                    if (ASAUARTSerialIsrStr
                            ->remo_reg[ASAUARTSerialIsrStr->reg_address]
                            .func_p != NULL)
                        ASAUARTSerialIsrStr
                            ->remo_reg[ASAUARTSerialIsrStr->reg_address]
                            .func_p(0);

                    UARTS_tx0();
                }
                else {  // Read mode
                    UARTS_tx0();
                }
            }
            else {  // CheckSum不同，回Header (參考編號8)
                DEBUG_INFO("Checksum error\n");
                ASAUARTSerialIsrStr->sm_status = UARTS_SM_HEADER;
                ASAUARTSerialIsrStr->byte_counter = 0;
                // 關閉逾時中斷
                Timeout_ctl(timeout_IsrID, 0);
            }
            break;
    }

    // Reset the timeout ISR
    TimerCntStr_inst.timeoutISR_inst[timeout_IsrID].counter =
        TimerCntStr_inst.timeoutISR_inst[timeout_IsrID].time_limit;
    timeout_flag = 0;
}
void UARTS_tx0() {
    switch (ASAUARTSerialIsrStr->sm_status) {
        case UARTS_SM_BYTES:
            // Read mode
            DEBUG_INFO("Process read mode\n");
            /*
            if (timeout_flag) {      // Timeout ISR，回ADDR
                ASAUARTSerialIsrStr->sm_status = UARTS_SM_ADDR;
                ASAUARTSerialIsrStr->result_message = HAL_ERROR_TIMEOUT;
                // 關閉逾時中斷
                Timeout_ctl(timeout_IsrID, 0);
                break;
            }
            */
            /**** Write the Data to Master ****/
            UARTS_Inst.write_byte(
                ASAUARTSerialIsrStr->remo_reg[ASAUARTSerialIsrStr->reg_address]
                    .data_p[ASAUARTSerialIsrStr->byte_counter]);
            ASAUARTSerialIsrStr->check_sum +=
                ASAUARTSerialIsrStr->remo_reg[ASAUARTSerialIsrStr->reg_address]
                    .data_p[ASAUARTSerialIsrStr->byte_counter];
            DEBUG_INFO(
                "Write Data <%u>\n",
                ASAUARTSerialIsrStr->remo_reg[ASAUARTSerialIsrStr->reg_address]
                    .data_p[ASAUARTSerialIsrStr->byte_counter]);
            ASAUARTSerialIsrStr->byte_counter++;

            if (ASAUARTSerialIsrStr->byte_counter ==
                ASAUARTSerialIsrStr->remo_reg[ASAUARTSerialIsrStr->reg_address]
                    .sz_reg) {
                /**** Write the Checksum to master ****/
                UARTS_Inst.write_byte(ASAUARTSerialIsrStr->check_sum);
                DEBUG_INFO("Write checksum <%u>\n",
                           ASAUARTSerialIsrStr->check_sum);

                ASAUARTSerialIsrStr->byte_counter = 0;
                ASAUARTSerialIsrStr->sm_status = UARTS_SM_HEADER;
                // 關閉逾時中斷
                Timeout_ctl(timeout_IsrID, 0);
            }

            break;

        case UARTS_SM_CHKSUM:
            /**** Write the Response Header to Master ****/
            UARTS_Inst.write_byte(RSP_HEADER);

            if (ASAUARTSerialIsrStr->rw_mode == 0) {
                ASAUARTSerialIsrStr->sm_status = UARTS_SM_HEADER;
                ASAUARTSerialIsrStr->byte_counter = 0;
                // 關閉逾時中斷
                Timeout_ctl(timeout_IsrID, 0);
            }
            else {  // read mode

                ASAUARTSerialIsrStr->sm_status = UARTS_SM_BYTES;
                ASAUARTSerialIsrStr->byte_counter = 0;
                ASAUARTSerialIsrStr->check_sum = 0;
            }
            break;
    }
}
