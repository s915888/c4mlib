/**
 * @file debug.h
 * @author LiYu87
 * @date 2019.03.27
 * @brief 提供函式庫開發人員DEBUG的輸出入介面
 *
 * 需在.h檔中引用
 * 並在測試.c檔最前面加上 #define USE_C4MLIB_DEBUG
 */
#ifndef C4MLIB_DEBUG_DEBUG_H
#define C4MLIB_DEBUG_DEBUG_H

#ifdef USE_C4MLIB_DEBUG
#    define DEBUG_INFO(format, args...) printf("[DEBUG] " format, ##args)
#else
#    define DEBUG_INFO(args...)
#endif

#endif  // C4MLIB_DEBUG_DEBUG_H
