/**
 * @file hal_spi.h
 * @author Deng Xiang-Guan
 * @date 2019.1.22
 * @brief Create a hardware abstraction layer to separate hardware dependent.
 */

#ifndef C4MLIB_HARDWARE_HAL_SPI_H
#define C4MLIB_HARDWARE_HAL_SPI_H

// TODO: 確認抽象層設計合理 @LIYU87

#define SPI_TIMEOUT_TIME 50UL
#define SPI_CF_MAX_BITS 0x07
#define SPI_CF_MASK 0x07
#define SPI_CF_MSB_MAX_BITS 0x1F
#define SPI_CF_MSB_MASK 0x1F

#include <stdint.h>

typedef struct {
    void (*init)(void);
    uint8_t (*spi_swap)(uint8_t data);
    void (*spi_multi_swap)(uint8_t* trm_data, uint8_t* rec_data,
                           uint8_t sizeOfData);
    void (*spi_compelete_cb)(void);
    void (*write_byte)(uint8_t data);
    void (*write_multi_bytes)(uint8_t* data_p, uint8_t sizeOfData);
    uint8_t (*read_byte)(void);
    void (*read_multi_bytes)(uint8_t* data_p, uint8_t sizeOfData);
    void (*enable_cs)(char id);
    void (*disable_cs)(char id);
    uint8_t error_code;
} TypeOfMasterSPIInst;

typedef struct {
    void (*init)(void);
    void (*spi_compelete_cb)(void);
    void (*write_byte)(uint8_t data);
    uint8_t (*read_byte)(void);
    uint8_t* echo_p;
    uint8_t error_code;
} TypeOfSlaveSPIInst;

TypeOfMasterSPIInst SPI_MasterInst;
TypeOfSlaveSPIInst SPI_SlaveInst;

#endif /* C4MLIB_HARDWARE_HAL_SPI_H */
