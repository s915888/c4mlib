#include "c4mlib/hardware/src/hardware.h"
#include "c4mlib/stdio/src/stdio.h"

#include <avr/interrupt.h>

volatile uint16_t count;
volatile uint16_t s;

void null_func(void) {
    ;
}

void timer2_isr() {
    count++;
    if (count == 1000) {
        s++;
        count = 0;
    }
}

int main() {
    // ASA_STDIO_init();

    printf("hardware timer test - exist\n");
    printf("    all return should be 0\n");

    uint8_t data = 0;
    uint8_t chk[5];
    chk[0] = TIM_set(200, 0xFF, 0, 0);
    chk[1] = TIM_fpt(200, 0xFF, 0, 0);
    chk[2] = TIM_put(0, 1, &data);
    chk[3] = TIM_get(20, 1, &data);
    // chk[4] = TIM_isr(0, null_func);

    printf("    return of M128_TIM_set is %d\n", chk[0]);
    printf("    return of M128_TIM_fpt is %d\n", chk[1]);
    printf("    return of M128_TIM_put is %d\n", chk[2]);
    printf("    return of M128_TIM_get is %d\n", chk[3]);
    // printf("    return of M128_TIM_isr is %d\n", chk[4]);

    printf("hardware timer test - timer2 1sec int\n");
    // 11059200/((2*64+1)*80)
    uint8_t ocr = 172;
    TIM_set(222, 0x07, 0, 3);  // devider = 64
    TIM_set(222, 0x48, 3, 1);  // CTC mode
    TIM_put(10, 1, &ocr);      // set ocr2
    TIM_set(207, 0x80, 7, 1);  // enable int
    // TIM_isr(2, timer2_isr);

    sei();
    uint16_t pre_s = s;
    while (1) {
        if (pre_s == s) {
            continue;
        }
        else {
            pre_s = s;
            printf("    count = %d\n", s);
        }
    }

    return 0;
}
