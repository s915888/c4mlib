/**
 * @file isr_func.h
 * @author LiYu87
 * @date 2019.04.01
 * @brief 提供標準的 ISR FUNCTION 型態。
 *
 * @priority 0
 */

// TODO: 完成此份文件之註解

#ifndef C4MLIB_INTERRUPT_ISR_FUNC_H
#define C4MLIB_INTERRUPT_ISR_FUNC_H

#include "c4mlib/macro/src/std_type.h"

/* Public Section Start */
typedef struct {
    volatile uint8_t postSlot;  // POST是否有新中斷之公告欄
    volatile uint8_t enable;  // 禁致能控制決定本中斷中執行函式是否致能
    Func_t func_p;            // 中斷中執行函式
    void* funcPara_p;  // 中斷中執行函式之傳參
} TypeOfISRFunc;
/* Public Section End */

#endif  // C4MLIB_INTERRUPT_ISR_FUNC_H
